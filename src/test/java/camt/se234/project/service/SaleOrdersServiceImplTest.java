package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrdersServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;
    static List<SaleOrder> mockSaleOrders;

    @BeforeClass
    public static void setUpTestData() {
        List<Product> mockProduct = new ArrayList<>();

        //mockProducts
        mockProduct.add(Product.builder().id(1L).productId("A01").name("SURFACE GO").description("SURFACE GO 128GB/8GB LTE (KAZ-00013)+Signature Type Cover Platinum (CEI-60).").imageLocation("/img/01.jpg").price(39900.00).build());
        mockProduct.add(Product.builder().id(2L).productId("A02").name("DELL Inspiron 5480").description("i7-8565U/8G/128G+1TB/MX150 2GB/14/SL/2Y").imageLocation("/img/02.jpg").price(31990.00).build());
        mockProduct.add(Product.builder().id(3L).productId("A03").name("USB C to HDMI Adaptor").description("This proprietary HDMI adapter lets you share photos, video, and presentations in a way that's larger than life. Plug into HDMI compatible displays, monitors, or projectors.").imageLocation("/img/03.jpg").price(1590.00).build());

        //mockSaleTransaction
        List<SaleTransaction> mockSaleTransaction = new ArrayList<>();
        mockSaleTransaction.add(SaleTransaction.builder().id(522L).transactionId("T001").product(mockProduct.get(0)).amount(5).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(523L).transactionId("T002").product(mockProduct.get(1)).amount(1).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(524L).transactionId("T003").product(mockProduct.get(2)).amount(2).build());

        //mockSaleOrders
        mockSaleOrders = new ArrayList<>();
        mockSaleOrders.add(SaleOrder.builder().id(1L).saleOrderId("S001").transactions(
                new ArrayList<SaleTransaction>()
                    {{  add(mockSaleTransaction.get(0));
                        add(mockSaleTransaction.get(1));
                    }}
                    ).build());

        mockSaleOrders.add(SaleOrder.builder().id(2L).saleOrderId("S002").transactions(
                new ArrayList<SaleTransaction>()
                {{  add(mockSaleTransaction.get(1));
                    add(mockSaleTransaction.get(2));
                }}
        ).build());

        mockSaleOrders.add(SaleOrder.builder().id(3L).saleOrderId("S003").transactions(
                new ArrayList<SaleTransaction>()
                {{  add(mockSaleTransaction.get(0));
                    add(mockSaleTransaction.get(2));
                }}
        ).build());
    }

    @Before
    public void setUp() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);

        when(orderDao.getOrders()).thenReturn(mockSaleOrders);
    }

    @Test
    public void testGetSaleOrders() {
        assertThat(saleOrderService.getSaleOrders(), hasItems(mockSaleOrders.get(0)));
    }

    @Test
    public void testGetAverageSaleOrderPrice() {
        assertThat(saleOrderService.getAverageSaleOrderPrice(), closeTo(156446,1));
    }
}
