package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {
    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setUp() {
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void testGetAllProducts() {
        List<Product> mockProduct = buildMockProduct();
        when(productDao.getProducts()).thenReturn(mockProduct);

        // check size
        assertThat(productService.getAllProducts().size(), is(mockProduct.size()));

        // check all product that return from getAllProduct
        List<Product> tempProduct = productService.getAllProducts();
        for (int i = 0; i < mockProduct.size(); i++) {
            assertThat(mockProduct.get(i), is(tempProduct.get(i)));
        }
    }

    @Test
    public void testGetAvailableProducts() {
        List<Product> mockProduct = buildMockProduct();
        when(productDao.getProducts()).thenReturn(mockProduct);
        List<Product> mockAvailableProduct = buildMockAvailableProduct();

        // check size
        assertThat(productService.getAvailableProducts().size(), is(3));

        // check all product that return from getAvailableProduct
        for (int i = 0; i < mockAvailableProduct.size(); i++) {
            assertThat(productService.getAvailableProducts().get(i), is(mockAvailableProduct.get(i)));
        }
    }

    @Test
    public void testGetUnavailableProductSize() {
        List<Product> mockProduct = buildMockProduct();
        when(productDao.getProducts()).thenReturn(mockProduct);
        assertThat(productService.getUnavailableProductSize(), is(1));
    }

    public List<Product> buildMockProduct() {
        List<Product> mockProduct = new ArrayList<>();
        mockProduct.add(Product.builder()
                .productId("A01")
                .name("SURFACE GO")
                .description("SURFACE GO 128GB/8GB LTE (KAZ-00013)+Signature Type Cover Platinum (CEI-60).")
                .imageLocation("/img/01.jpg")
                .price(39900.00)
                .build());
        mockProduct.add(Product.builder()
                .productId("A02")
                .name("DELL Inspiron 5480")
                .description("i7-8565U/8G/128G+1TB/MX150 2GB/14\"/SL/2Y")
                .imageLocation("/img/02.jpg")
                .price(0.00)
                .build());
        mockProduct.add(Product.builder()
                .productId("A03")
                .name("USB C to HDMI Adaptor")
                .description("This proprietary HDMI adapter lets you share photos, video, and presentations in a way that's larger than life. Plug into HDMI compatible displays, monitors, or projectors.")
                .imageLocation("/img/03.jpg")
                .price(1590.00)
                .build());
        mockProduct.add(Product.builder()
                .productId("A04")
                .name("ASUS Curved Gaming Monitor 27\" XG27VQ")
                .description("Screen size 27\" ROG Strix XG27VQ Curved Gaming Monitor – 27 inch Full HD (1920x1080)")
                .imageLocation("/img/04.jpg")
                .price(16690.00)
                .build());
        return mockProduct;
    }

    public List<Product> buildMockAvailableProduct() {
        List<Product> mockProduct = new ArrayList<>();
        mockProduct.add(Product.builder()
                .productId("A01")
                .name("SURFACE GO")
                .description("SURFACE GO 128GB/8GB LTE (KAZ-00013)+Signature Type Cover Platinum (CEI-60).")
                .imageLocation("/img/01.jpg")
                .price(39900.00)
                .build());
        mockProduct.add(Product.builder()
                .productId("A03")
                .name("USB C to HDMI Adaptor")
                .description("This proprietary HDMI adapter lets you share photos, video, and presentations in a way that's larger than life. Plug into HDMI compatible displays, monitors, or projectors.")
                .imageLocation("/img/03.jpg")
                .price(1590.00)
                .build());
        mockProduct.add(Product.builder()
                .productId("A04")
                .name("ASUS Curved Gaming Monitor 27\" XG27VQ")
                .description("Screen size 27\" ROG Strix XG27VQ Curved Gaming Monitor – 27 inch Full HD (1920x1080)")
                .imageLocation("/img/04.jpg")
                .price(16690.00)
                .build());
        return mockProduct;
    }

}