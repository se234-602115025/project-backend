package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AuthenticationServiceImplTest {
   UserDao userDao;
   AuthenticationServiceImpl authenticationService;

   @Before
    public void setUp() {
       userDao = mock(UserDao.class);
       authenticationService = new AuthenticationServiceImpl();
       authenticationService.setUserDao(userDao);

   }

   @Test
    public void testAuthenticateThatUserExist() {
       User mockuser = new User(1L,"satoshi", "1150", "Miner");
       when(userDao.getUser("satoshi","1150")).thenReturn(mockuser);

      assertThat(authenticationService.authenticate("satoshi", "1150"), is(mockuser));
   }

   @Test
   public void testAuthenticateThatUserDoesNotExist() {
      User mockuser = new User(1L,"satoshi", "1150", "Miner");
      when(userDao.getUser("satoshi","1150")).thenReturn(mockuser);

      assertThat(authenticationService.authenticate("sashito", "5011"), is(nullValue()));

   }
}
